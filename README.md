Module for sending SMS via Ukrainian SMS Gateway "Turbo SMS". The module uses the subscribers list, that determined the SMS's subscribers by specific realty operation types: rent, sale and exchange.

This module requires the registration at the TurboSMS https://turbosms.ua/ site for use its GW for sending SMS.

Module for Drupal 7.